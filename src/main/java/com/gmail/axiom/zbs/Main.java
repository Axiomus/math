package com.gmail.axiom.zbs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите число:");
        double number = Double.parseDouble(reader.readLine());
        System.out.println("Sin(" + number + ")/Cos(" + number + ") = " + Math.sinX(number) / Math.cosX(number));
    }
}
