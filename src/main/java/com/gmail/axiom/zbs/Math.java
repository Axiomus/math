package com.gmail.axiom.zbs;

public class Math {

    public Math() {
    }

    public static double sinX(double x){
        return java.lang.Math.sin(x);
    }

    public static double cosX(double x){
        return java.lang.Math.cos(x);
    }
}
